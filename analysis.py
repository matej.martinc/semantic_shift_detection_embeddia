import pandas as pd
import plotly.express as px
import pickle
from sklearn.metrics.pairwise import cosine_similarity
import editdistance
import argparse
import sys



def avg_emb(words, all_embeds, year=None):
    if not isinstance(words, list):
        words = [words]
    if year is None:
        embeds = []
        for y in all_embeds.keys():
            y_embeds = []
            for w in words:
                if w in all_embeds[y]:
                    y_embeds.append(all_embeds[y][w])
                else:
                    print(f'Warning: {w} not in vocabulary of slice {y}!')
            embeds.extend(y_embeds)
    else:
        embeds = [all_embeds[year][w] for w in words if w in all_embeds[year]]

    if len(embeds) == 0:
        print(f'None of the words in the list are in the vocabulary, exiting')
        sys.exit()
        return None
    return sum(embeds) / len(embeds)


def check_editdistance(target_word_list, compare_word, treshold=0.5):
    farEnough = True
    for w in target_word_list:
        ed = 1 - (float(editdistance.eval(w, compare_word)) / max(len(w), len(compare_word)))
        if ed > treshold:
            farEnough = False
            break
    return farEnough



def get_cos_dist(neighbors, word, years, vocab_vectors):

    all_data = []

    for year in years:
        if isinstance(word, list):
            word_emb = avg_emb(word, vocab_vectors, year)
        else:
            word_emb = vocab_vectors[year][word]

        for neigh in neighbors:
            embeds = []
            for w in neigh:
                if w in vocab_vectors[year]:
                    embeds.append(vocab_vectors[year][w])
                else:
                    print(f'{w} not in chunk {year}.')

            if len(embeds) > 0:
                neigh_embed = sum(embeds) / len(embeds)
                cs = cosine_similarity(word_emb, neigh_embed)[0][0]
                all_data.append([neigh[0], year, cs])

    return all_data

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))

def get_most_related_words(word, vocab_vectors, years, closest=50):

    if isinstance(word, list):
        word_emb = avg_emb(word, vocab_vectors)
    else:
        word_emb = avg_emb([word], vocab_vectors)

    neigh = []
    all_words = []
    for year in years:
        yearly_vocab_vectors = vocab_vectors[year]
        year_words = list(set([w for w in yearly_vocab_vectors.keys() if check_editdistance(word, w)]))
        if all_words:
            all_words = intersection(all_words, year_words)
        else:
            all_words = yearly_vocab_vectors
    all_words = list(set(all_words))
    #print(len(all_words),all_words[:100])

    for w in all_words:
        w_emb = avg_emb(w, vocab_vectors)
        cs = cosine_similarity(word_emb, w_emb)
        # print(k, previous_k, cs)
        neigh.append((w, cs))

    neigh = sorted(neigh, key=lambda x: x[1], reverse=True)
    neigh = [k for (k,v) in neigh if len(k) > 2][:closest]

    print('Nearest words to ', word)
    for n in neigh:
        print(n)
    return neigh

def get_biggest_diff(word, years, vocab_vectors, related_words, num_words=10):

    diffs = {}
    related_words = set(related_words)
    incomplete = set()

    all_words = []
    for year in years:
        yearly_vocab_vectors = vocab_vectors[year]
        year_words = list(set([w for w in yearly_vocab_vectors.keys() if w in related_words and check_editdistance(word, w)]))
        all_words.extend(year_words)
    all_words = list(set(all_words))

    for i in range(len(years) - 1):
        year1 = years[i]
        year2 = years[i + 1]

        if isinstance(word, list):
            word_emb1 = avg_emb(word, vocab_vectors, year1)
            word_emb2 = avg_emb(word, vocab_vectors, year2)
        else:
            word_emb1 = vocab_vectors[year1][word]
            word_emb2 = vocab_vectors[year2][word]

        for w in all_words:
            try:
                w_emb1 = vocab_vectors[year1][w]
                w_emb2 = vocab_vectors[year2][w]
                cs1 = cosine_similarity(word_emb1, w_emb1)[0][0]
                cs2 = cosine_similarity(word_emb2, w_emb2)[0][0]
                diff = abs(cs1 - cs2)

                if w in diffs:
                    diffs[w] += diff
                else:
                    diffs[w] = diff
            except:
                incomplete.add(w)
                pass

    diffs = list(diffs.items())
    diffs = [(k,v) for (k,v) in diffs if k not in incomplete]

    diffs = sorted(diffs, key=lambda x: x[1], reverse=True)
    print("Most changed words: ", diffs[:1000])

    most_changed = [[x] for (x,y) in diffs[:num_words]]
    return most_changed



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--target_word", default="donald trump", type=str,
                        help="Word or phrase you are interested in analyzing")
    parser.add_argument('--embeddings_path', type=str,
                        help='Path to temporal embeddings',
                        default='embeddings/croatian-fine-tuned.pickle')
    parser.add_argument('--slices', type=str,
                        help='Name of corpus slices separated by ";". Should correspond to the number of datasets',
                        default='2015;2016;2017;2018;2019')
    args = parser.parse_args()

    word = args.target_word.split()
    years = args.slices.split(';')
    path = args.embeddings_path


    with open(path, 'rb') as f:
        vocab_vectors = pickle.load(f)

    related_words = get_most_related_words(word, vocab_vectors, years, closest=10)
    most_changed = get_biggest_diff(word, years, vocab_vectors, related_words, num_words=5)
    data = get_cos_dist(most_changed, word, years, vocab_vectors)

    df = pd.DataFrame(data)
    df.columns = ['word', 'year', 'cosine similarity']
    df['year'] =  df['year'].map(lambda x: 'slice ' + x)

    fig = px.line(df, x='year', y='cosine similarity', color='word')
    fig.show()
