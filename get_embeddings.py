import torch
from transformers import BertTokenizer, BertModel

import numpy as np
import pickle
import gc
import re
import pandas as pd
import argparse
from nltk import sent_tokenize
from lemmagen3 import Lemmatizer


def remove_url(text, replace_token):
    regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    return re.sub(regex, replace_token, text)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_shifts(input_path):
    shifts_dict = {}
    df_shifts = pd.read_csv(input_path, sep=',', encoding='utf8')
    for idx, row in df_shifts.iterrows():
        shifts_dict[row['word']] = row['shift_index']
    return shifts_dict

def tokens_to_batches(ds, tokenizer, batch_size, max_length):

    batches = []
    batch = []
    batch_counter = 0

    print('Dataset: ', ds)
    counter = 0

    with open(ds, 'r', encoding='utf8') as f:

        for line in f:
            counter += 1
            #if counter % 100 == 0:
            #    break


            if counter % 1000 == 0:
                print('Num articles: ', counter)

            text = line.replace('"', '').replace("'", '')
            text = remove_url(text, '')
            text = " ".join(text.split())
            sents = sent_tokenize(text)
            tokenized_text = []
            for sent in sents:
                tokenized_sent = tokenizer.tokenize(sent)
                if len(sent) < max_length - 2 and len(sent) > 1:
                    if len(tokenized_sent) + len(tokenized_text) > max_length - 2:
                        input_sequence = ["[CLS]"] + tokenized_text + ["[SEP]"]
                        tokenized_text = []
                        if len(input_sequence) < max_length:
                            pad = ['[PAD]'] * (max_length - len(input_sequence))
                            input_sequence = input_sequence + pad
                        batch_counter += 1
                        indexed_tokens = tokenizer.convert_tokens_to_ids(input_sequence)
                        batch.append((indexed_tokens, input_sequence))
                        if batch_counter % batch_size == 0:
                            batches.append(batch)
                            batch = []
                    else:
                        tokenized_text.extend(tokenized_sent)

            if len(tokenized_text) > 1 and len(tokenized_text) < max_length - 2:
                input_sequence = ["[CLS]"] + tokenized_text + ["[SEP]"]
                if len(input_sequence) < max_length:
                    pad = ['[PAD]'] * (max_length - len(input_sequence))
                    input_sequence = input_sequence + pad
                batch_counter += 1
                indexed_tokens = tokenizer.convert_tokens_to_ids(input_sequence)
                batch.append((indexed_tokens, input_sequence))
                if batch_counter % batch_size == 0:
                    batches.append(batch)
                    batch = []

    print()
    print('Tokenization done!')
    print('len batches: ', len(batches))

    return batches


def get_token_embeddings(batches, model, batch_size):

    token_embeddings = []
    tokenized_text = []
    counter = 0

    for batch in batches:
        counter += 1
        if counter % 1000 == 0:
            print('Generating embedding for batch: ', counter)
        lens = [len(x[0]) for x in batch]
        max_len = max(lens)
        tokens_tensor = torch.zeros(batch_size, max_len, dtype=torch.long).cuda()
        segments_tensors = torch.ones(batch_size, max_len, dtype=torch.long).cuda()
        batch_idx = [x[0] for x in batch]
        batch_tokens = [x[1] for x in batch]

        for i in range(batch_size):
            length = len(batch_idx[i])
            for j in range(max_len):
                if j < length:
                    tokens_tensor[i][j] = batch_idx[i][j]

        # Predict hidden states features for each layer
        with torch.no_grad():
            model_output = model(tokens_tensor, segments_tensors)
            encoded_layers = model_output[-1][-4:] #last four layers of the encoder


        for batch_i in range(batch_size):

            # For each token in the sentence...
            for token_i in range(len(batch_tokens[batch_i])):

                # Holds last 4 layers of hidden states for each token
                hidden_layers = []

                for layer_i in range(len(encoded_layers)):
                    # Lookup the vector for `token_i` in `layer_i`
                    vec = encoded_layers[layer_i][batch_i][token_i]

                    hidden_layers.append(vec)

                hidden_layers = torch.sum(torch.stack(hidden_layers)[-4:], 0).reshape(1, -1).detach().cpu().numpy()

                token_embeddings.append(hidden_layers)
                tokenized_text.append(batch_tokens[batch_i][token_i])

    return token_embeddings, tokenized_text


def average_save_and_print(vocab_vectors, save_path, limit=5):
    filtered_vocab_vectors = {}
    path = save_path.split('.')[0]
    path = path + '-vocab.tsv'
    vocab = []
    with open(path, 'w', encoding='utf8') as f:
        f.write('word\tyear\tcount\n')
        for year, words in vocab_vectors.items():
            filtered_vocab_vectors[year] = {}
            counter = 0
            for k, v in words.items():
                if len(v) == 2:
                    if v[1] >= limit:
                        vocab.append(f'{k}\t{year}\t{v[1]}')
                        avg = v[0] / v[1]
                        filtered_vocab_vectors[year][k] = avg
                        counter += 1
            print(f'Num. embeddings in {year}: {counter}')
        f.write("\n".join(vocab))

    with open(save_path, 'wb') as handle:
        pickle.dump(filtered_vocab_vectors, handle, protocol=pickle.HIGHEST_PROTOCOL)




def get_time_embeddings(embeddings_path, datasets, slices, tokenizer, model, batch_size, max_length, lemmatizer):

    vocab_vectors = {}

    for year, ds in zip(slices,datasets):

        all_batches = tokens_to_batches(ds, tokenizer, batch_size, max_length)
        print('Num. all batches', len(all_batches))
        chunked_batches = chunks(all_batches, 1000)
        num_chunk = 0

        for batches in chunked_batches:
            num_chunk += 1
            print('Chunk ', num_chunk)

            token_embeddings, tokenized_text = get_token_embeddings(batches, model, batch_size)

            splitted_tokens = []
            splitted_array = np.zeros((1, 768))
            prev_token = ""
            prev_array = np.zeros((1, 768))

            for i, token_i in enumerate(tokenized_text):

                array = token_embeddings[i]

                if token_i.startswith('##'):

                    if prev_token:
                        splitted_tokens.append(prev_token)
                        prev_token = ""
                        splitted_array = prev_array

                    splitted_tokens.append(token_i)
                    splitted_array += array

                else:
                    if i == len(tokenized_text) - 1 or not tokenized_text[i + 1].startswith('##'):
                        if lemmatizer is not None:
                            token_i = lemmatizer.lemmatize(token_i)
                        if year in vocab_vectors:
                            if token_i in vocab_vectors[year]:
                                vocab_vectors[year][token_i][0] += array
                                vocab_vectors[year][token_i][1] += 1
                            else:
                                vocab_vectors[year][token_i] = [array, 1]
                        else:
                            vocab_vectors[year] = {}
                            vocab_vectors[year][token_i] = [array, 1]

                    if splitted_tokens:
                        sarray = splitted_array / len(splitted_tokens)
                        stoken_i = "".join(splitted_tokens).replace('##', '')
                        if lemmatizer is not None:
                            stoken_i = lemmatizer.lemmatize(stoken_i)

                        if year in vocab_vectors:
                            if stoken_i in vocab_vectors[year]:
                                vocab_vectors[year][stoken_i][0] += sarray
                                vocab_vectors[year][stoken_i][1] += 1
                            else:
                                vocab_vectors[year][stoken_i] = [sarray, 1]
                        else:
                            vocab_vectors[year] = {}
                            vocab_vectors[year][stoken_i] = [sarray, 1]

                        splitted_tokens = []
                        splitted_array = np.zeros((1, 768))

                    prev_array = array
                    prev_token = token_i

            del token_embeddings
            del tokenized_text
            del batches
            gc.collect()

        print('Sentence embeddings generated.')

    print("Length of vocab after training: ", len(vocab_vectors.items()))

    average_save_and_print(vocab_vectors, embeddings_path)
    del vocab_vectors
    gc.collect()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", type=int, default=24)
    parser.add_argument("--max_length", type=int, default=256)
    parser.add_argument("--model_name", default="EMBEDDIA/finest-bert", type=str,
                        help="The model checkpoint for weights initialization.")
    parser.add_argument('--path_to_model', type=str,
                        help='Paths to the fine-tuned BERT model',
                        default='models/estonian/checkpoint-81445/pytorch_model.bin')
    parser.add_argument('--path_to_datasets', type=str,
                        help='Paths to each of the time period specific corpus separated by ;',
                        default='data/estonian/estonian_2015.txt;data/estonian/estonian_2016.txt;data/estonian/estonian_2017.txt;'
                                'data/estonian/estonian_2018.txt;data/estonian/estonian_2019.txt',)
    parser.add_argument('--slices', type=str,
                        help='Name of corpus slices separated by ";". Should correspond to the number of datasets',
                        default='2015;2016;2017;2018;2019')
    parser.add_argument('--embeddings_path', type=str,
                        help='Path to output time embeddings',
                        default='embeddings/estonian-fine-tuned.pickle')
    parser.add_argument('--lang', type=str,
                        help='Language, can be croatian, estonian or english',
                        default='estonian')
    args = parser.parse_args()

    datasets = args.path_to_datasets.split(';')
    slices = args.slices.split(';')

    tokenizer = BertTokenizer.from_pretrained(args.model_name)
    #you can also use a pretrained not fine-tuned model
    #model = BertModel.from_pretrained(args.model_name, output_hidden_states=True)
    state_dict = torch.load(args.path_to_model)
    model = BertModel.from_pretrained(args.model_name, state_dict=state_dict, output_hidden_states=True)

    model.cuda()
    model.eval()

    embeddings_path = args.embeddings_path

    if args.lang == 'english':
        lemmatizer = Lemmatizer('en')
    elif args.lang == 'estonian':
        lemmatizer = Lemmatizer('et')
    elif args.lang == 'croatian':
        lemmatizer = Lemmatizer('hr')

    get_time_embeddings(embeddings_path, datasets, slices, tokenizer, model, args.batch_size, args.max_length, lemmatizer)



