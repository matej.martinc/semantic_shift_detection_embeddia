# Code for diachronic analysis based on the paper 'Leveraging Contextual Embeddings for Detecting Diachronic Semantic Shift' published in proceedings of the LREC 2020 conference #

Please cite the following paper [[bib](https://gitlab.com/matej.martinc/semantic_shift_detection/-/blob/master/bibtex.js)] if you use this code:

Matej Martinc, Petra Kralj Novak and Senja Pollak. Leveraging Contextual Embeddings for Detecting Diachronic Semantic Shift. In Proceedings of the Twelfth International Conference on Language Resources and Evaluation (LREC 2020). Marseille, France.


## Installation, documentation ##

Instructions for installation assume the usage of PyPI package manager.<br/>
To get the source code, train data and trained embeddings, clone the project from the repository with 'git clone https://gitlab.com/matej.martinc/semantic_shift_detection_embeddia' <br/>
Install dependencies if needed: pip install -r requirements.txt

### To fine-tune the BERT language model on the new data for domain adaptation: ###

```
python fine-tuning.py --train_data_file path_to_train_data --output_dir path_to_output_model_directory --eval_data_file path_to_train_data --model_name_or_path huggingface_model_designator(e.g., bert-base-uncased) --mlm --do_train --do_eval --evaluate_during_training
```

See default values of each argument for more info. Also, see dataset examples in folder 'data' for details on the input format. <br/>

### Generate time specific representation (i.e., a distinct embedding representation for each word in each time period) for each word using the fine-tuned language model: ### 

```
python get_embeddings.py --model_name huggingface_model_designator(e.g., bert-base-uncased) --path_to_model path_to_fine_tuned_model --path_to_datasets path_to_diahronic_datasets_separated_by_; --slices names_for_time_slices_separated_by_; --embeddings_path output_embeddings_path --lang language(english, estonian or croatian)
```

Again, see default values of each argument for more info.

### Obtain a diacronic change analysis for a specific word or phrase ###

```
python analysis.py --target_word word_or_phrase_of_interest --embeddings_path path_to_embeddings --slices names_for_time_slices_separated_by_;
```

See default values of each argument for more info. <br/><br/>

This repository also contains already trained diachronic embeddings for Croatian and Estonian, trained on news articles 
from 2015 until 2019 and split into yearly temporal slices.  <br/><br/>

### Finally, see jupyter notebook 'analysis.ipynb' to understand the details of how the diachronic analysis script works. ###





## Contributors to the code ##

Matej Martinc<br/>

* [Knowledge Technologies Department](http://kt.ijs.si), Jožef Stefan Institute, Ljubljana
