import pandas as pd
import string
import json
from collections import defaultdict
import random
from nltk import sent_tokenize


def build_dataset():
    df = pd.read_csv('STY_24sata_articles_hr_002.csv', encoding='utf8', sep=',')
    output_train = open('croatian_train.json', 'w', encoding='utf8')
    output_test = open('croatian_test.json', 'w', encoding='utf8')

    year_slices = defaultdict(list)
    count_years = defaultdict(int)
    slices = ['2015', '2016', '2017', '2018', '2019']
    all_dates = []


    for df, row in df.iterrows():

        date_all = row['date_created']
        all_dates.append(row['date_created'])
        year = date_all[:4]
        if year in slices:
            count_years[year] += 1
            title = str(row['title'])
            lead = str(row['lead']).strip()
            if lead and lead !='nan' and lead[-1] not in string.punctuation:
                lead = lead + '.'
            content = str(row['content'])

            title = " ".join(title.split())
            lead = " ".join(lead.split())
            content = " ".join(content.split())
            if lead and lead != 'nan':
                content = str(lead + ' ' + content)
            text = title + '. ' + content
            year_slices[year].append(text)
    print(count_years)
    all_dates = sorted(all_dates)
    print(all_dates[0])
    print(all_dates[-1])


    all_docs = []
    print('Making year slices')
    for year, docs in year_slices.items():
        output_path = open('croatian_' + year + '.txt', 'w', encoding='utf8')
        random.shuffle(docs)
        for doc in docs[:20000]:
            all_docs.append(doc)
            output_path.write(doc + '\n')
        output_path.close()

    print('Making lm train test')
    random.shuffle(all_docs)
    test_idx = int(len(all_docs) * 0.9)
    for idx, doc in enumerate(all_docs):
        for sent in sent_tokenize(doc):
            if idx < test_idx:
                output_train.write(sent + '\n')
            else:
                output_test.write(sent + '\n')
    output_train.close()
    output_test.close()


build_dataset()

