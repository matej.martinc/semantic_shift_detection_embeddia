import json
from bs4 import BeautifulSoup
from lemmagen3 import Lemmatizer
from nltk import sent_tokenize, RegexpTokenizer
tokenizer = RegexpTokenizer('[\(]|[\w-]+|\$[\d\.]+|\S+')
import random
import string



def build_dataset():
    estonian_files = ['ee_2015.json', 'ee_2016.json', 'ee_2017.json', 'ee_2018.json', 'ee_2019.json']

    output_train = open('train.txt', 'w', encoding='utf8')
    output_test = open('test.txt', 'w', encoding='utf8')

    count_docs = 0

    all_docs = []

    for path in estonian_files:
        with open(path, 'r', encoding='utf8') as f:
            text = json.load(f)
            year = path.split('.')[0].split('_')[1]
            output_path = open('estonian_' + year + '.txt', 'w', encoding='utf8')
            for d in text:
                if 'channelLanguage' in d:
                    if d['channelLanguage'] == 'nat':
                        # print('Related tags:', d['relatedTags'])
                        count_docs += 1
                        if count_docs % 1000 == 0:
                             print(count_docs)

                        title = BeautifulSoup(d['title']).get_text()
                        lead = BeautifulSoup(d['lead']).get_text()
                        if len(lead) > 0 and lead[-1] not in string.punctuation:
                            lead = lead + '.'
                        content = str(d['bodyText'])

                        text = title + '. ' + lead + ' ' + content
                        text = " ".join(text.split())
                        output_path.write(text + '\n')
                        sents = sent_tokenize(text)
                        all_docs.append(sents)
            output_path.close()

    print('Making lm train test')
    random.shuffle(all_docs)
    test_idx = int(len(all_docs) * 0.9)
    for idx, doc in enumerate(all_docs):
        for sent in doc:
            if idx < test_idx:
                output_train.write(sent + '\n')
            else:
                output_test.write(sent + '\n')

    output_train.close()
    output_test.close()


build_dataset()



